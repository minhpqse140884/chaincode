/*
 * SPDX-License-Identifier: Apache-2.0
 */

package org.hyperledger.fabric.samples.assettransfer;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.ThrowableAssert.catchThrowable;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hyperledger.fabric.samples.BGSSContract.ContractBGSS;

import org.hyperledger.fabric.contract.Context;
import org.hyperledger.fabric.shim.ChaincodeException;
import org.hyperledger.fabric.shim.ChaincodeStub;
import org.hyperledger.fabric.shim.ledger.KeyValue;
import org.hyperledger.fabric.shim.ledger.QueryResultsIterator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public final class AssetTransferTest {

    private final class MockKeyValue implements KeyValue {

        private final String key;
        private final String value;

        MockKeyValue(final String key, final String value) {
            super();
            this.key = key;
            this.value = value;
        }

        @Override
        public String getKey() {
            return this.key;
        }

        @Override
        public String getStringValue() {
            return this.value;
        }

        @Override
        public byte[] getValue() {
            return this.value.getBytes();
        }

    }

    private final class MockAssetResultsIterator implements QueryResultsIterator<KeyValue> {

        private final List<KeyValue> contractList;

        MockAssetResultsIterator() {
            super();
            contractList = new ArrayList<KeyValue>();

            contractList.add(new MockKeyValue("1L",
                    "{\"assetID\": \"asset1\", \"contractId\": 1L, \"actionParty\": 1L, \"fullName\": \"Tomoko\", \"address\": \"123 Blockchain Blvd\", \"quantity\": 12510, \"pricePerOunce\": 1250, \"totalAmount\": 1250, \"transactionType\": \"BUY\", \"createdAt\": \"1712847198\", \"confirmingParty\": \"BGSS Company\", \"goldUnit\": \"ABDC\", \"signatureActionParty\": \"CDBA\", \"signatureConfirmingParty\": \"SIGNED\", \"contractStatus\": \"DIGITAL_SIGNED\"}"));
            contractList.add(new MockKeyValue("2L",
                    "{\"assetID\": \"asset2\", \"contractId\": 1, \"actionParty\": 2L, \"fullName\": \"Tomoko\", \"address\": \"123 Blockchain Blvd\", \"quantity\": 12510, \"pricePerOunce\": 1250, \"totalAmount\": 1250, \"transactionType\": \"BUY\", \"createdAt\": \"1712847198\", \"confirmingParty\": \"BGSS Company\", \"goldUnit\": \"ABDC\", \"signatureActionParty\": \"CDBA\", \"signatureConfirmingParty\": \"SIGNED\", \"contractStatus\": \"DIGITAL_SIGNED\"}"));
            contractList.add(new MockKeyValue("3L",
                    "{\"assetID\": \"asset3\", \"contractId\": 3L, \"actionParty\": 2L, \"fullName\": \"Jane Doe\", \"address\": \"456 Crypto Road\", \"quantity\": 15000, \"pricePerOunce\": 1500, \"totalAmount\": 1500, \"transactionType\": \"SELL\", \"createdAt\": \"1712847200\", \"confirmingParty\": \"XYZ Corp\", \"goldUnit\": \"GRAMS\", \"signatureActionParty\": \"ABCD\", \"signatureConfirmingParty\": \"CONFIRMED\", \"contractStatus\": \"PENDING\"}"));
            contractList.add(new MockKeyValue("4L",
                    "{\"assetID\": \"asset4\", \"contractId\": 4, \"actionParty\": 1L, \"fullName\": \"Alice Johnson\", \"address\": \"789 Satoshi St\", \"quantity\": 13000, \"pricePerOunce\": 1300, \"totalAmount\": 1300, \"transactionType\": \"BUY\", \"createdAt\": \"1712847205\", \"confirmingParty\": \"DEF Inc\", \"goldUnit\": \"OZ\", \"signatureActionParty\": \"1234\", \"signatureConfirmingParty\": \"SIGNED\", \"contractStatus\": \"APPROVED\"}"));
            contractList.add(new MockKeyValue("5L",
                    "{\"assetID\": \"asset5\", \"contractId\": 5, \"actionParty\": 3L, \"fullName\": \"John Smith\", \"address\": \"1010 Ether Ave\", \"quantity\": 14000, \"pricePerOunce\": 1400, \"totalAmount\": 1400, \"transactionType\": \"BUY\", \"createdAt\": \"1712847210\", \"confirmingParty\": \"GHI LLC\", \"goldUnit\": \"KG\", \"signatureActionParty\": \"5678\", \"signatureConfirmingParty\": \"AUTHORIZED\", \"contractStatus\": \"VALIDATED\"}"));
            contractList.add(new MockKeyValue("6L",
                    "{\"assetID\": \"asset6\", \"contractId\": 6, \"actionParty\": 5L, \"fullName\": \"Mohammed Ali\", \"address\": \"1212 Blockchain Loop\", \"quantity\": 13500, \"pricePerOunce\": 1350, \"totalAmount\": 1350, \"transactionType\": \"SELL\", \"createdAt\": \"1712847215\", \"confirmingParty\": \"JKL Holdings\", \"goldUnit\": \"LB\", \"signatureActionParty\": \"91011\", \"signatureConfirmingParty\": \"VERIFIED\", \"contractStatus\": \"COMPLETED\"}"));
        }

        @Override
        public Iterator<KeyValue> iterator() {
            return contractList.iterator();
        }

        @Override
        public void close() throws Exception {
            // do nothing
        }

    }

    @Test
    public void invokeUnknownTransaction() {
        ContractBGSS contract = new ContractBGSS();
        Context ctx = mock(Context.class);

        Throwable thrown = catchThrowable(() -> {
            contract.unknownTransaction(ctx);
        });

        assertThat(thrown).isInstanceOf(ChaincodeException.class).hasNoCause()
                .hasMessage("Undefined contract method called");
        assertThat(((ChaincodeException) thrown).getPayload()).isEqualTo(null);

        verifyZeroInteractions(ctx);
    }

    @Nested
    class InvokeReadAssetTransaction {

//        @Test
//        public void whenAssetExists() {
//            AssetTransfer contract = new AssetTransfer();
//            Context ctx = mock(Context.class);
//            ChaincodeStub stub = mock(ChaincodeStub.class);
//            when(ctx.getStub()).thenReturn(stub);
//            when(stub.getStringState("1L"))
//                    .thenReturn("{\"contractId\": 1L, \"actionParty\": 1L, \"fullName\": \"Tomoko\", \"address\": \"123 Blockchain Blvd\", \"quantity\": 12510, \"pricePerOunce\": 12510, \"totalAmount\": 1250, \"transactionType\": \"BUY\", \"createdAt\": \"1712847198\", \"confirmingParty\": \"BGSS Company\", \"goldUnit\": \"GAM\", \"signatureActionParty\": \"CDBA\", \"signatureConfirmingParty\": \"SIGNED\", \"contractStatus\": \"DIGITAL_SIGNED\"}");
//
//            BigDecimal quantity = new BigDecimal(12510);
//            BigDecimal pricePerOunce = new BigDecimal(12510);
//            BigDecimal totalAmount = new BigDecimal(1250);
//
//            ContractDTO asset = contract.ReadAsset(ctx, "1L");
//
//            assertThat(asset).isEqualTo(new ContractDTO(1L, 1L, "Tomoko", "123 Blockchain Blvd", quantity, pricePerOunce, totalAmount, "BUY", new Date(), "BGSS Company", "GAM", "CDBA", "SIGNED", "DIGITAL_SIGNED"));
//        }

//        @Test
//        public void whenAssetDoesNotExist() {
//            AssetTransfer contract = new AssetTransfer();
//            Context ctx = mock(Context.class);
//            ChaincodeStub stub = mock(ChaincodeStub.class);
//            when(ctx.getStub()).thenReturn(stub);
//            when(stub.getStringState("asset1")).thenReturn("");
//
//            Throwable thrown = catchThrowable(() -> {
//                contract.ReadAsset(ctx, "1L");
//            });
//
//            assertThat(thrown).isInstanceOf(ChaincodeException.class).hasNoCause()
//                    .hasMessage("Asset asset1 does not exist");
//            assertThat(((ChaincodeException) thrown).getPayload()).isEqualTo("ASSET_NOT_FOUND".getBytes());
//        }
//    }

//    @Test
//    void invokeInitLedgerTransaction() {
//        AssetTransfer contract = new AssetTransfer();
//        Context ctx = mock(Context.class);
//        ChaincodeStub stub = mock(ChaincodeStub.class);
//        when(ctx.getStub()).thenReturn(stub);
//
//        contract.InitLedger(ctx);
//
//        InOrder inOrder = inOrder(stub);
//
//
//        inOrder.verify(stub).putStringState("1L", "{\"contractId\": 1L, \"actionParty\": 1L, \"fullName\": \"Tomoko\", \"address\": \"123 Blockchain Blvd\", \"quantity\": 12510, \"pricePerOunce\": 1250, \"totalAmount\": 1250, \"transactionType\": \"BUY\", \"createdAt\": \"1712847198\", \"confirmingParty\": \"BGSS Company\", \"goldUnit\": \"ABDC\", \"signatureActionParty\": \"CDBA\", \"signatureConfirmingParty\": \"SIGNED\", \"contractStatus\": \"DIGITAL_SIGNED\"}");
//        inOrder.verify(stub).putStringState("2L", "{\"contractId\": 1, \"actionParty\": 2L, \"fullName\": \"Tomoko\", \"address\": \"123 Blockchain Blvd\", \"quantity\": 12510, \"pricePerOunce\": 1250, \"totalAmount\": 1250, \"transactionType\": \"BUY\", \"createdAt\": \"1712847198\", \"confirmingParty\": \"BGSS Company\", \"goldUnit\": \"ABDC\", \"signatureActionParty\": \"CDBA\", \"signatureConfirmingParty\": \"SIGNED\", \"contractStatus\": \"DIGITAL_SIGNED\"}");
//        inOrder.verify(stub).putStringState("3L", "{\"contractId\": 3L, \"actionParty\": 2, \"fullName\": \"Jane Doe\", \"address\": \"456 Crypto Road\", \"quantity\": 15000, \"pricePerOunce\": 1500, \"totalAmount\": 1500, \"transactionType\": \"SELL\", \"createdAt\": \"1712847200\", \"confirmingParty\": \"XYZ Corp\", \"goldUnit\": \"GRAMS\", \"signatureActionParty\": \"ABCD\", \"signatureConfirmingParty\": \"CONFIRMED\", \"contractStatus\": \"PENDING\"}");
//        inOrder.verify(stub).putStringState("4L", "{\"contractId\": 4, \"actionParty\": 1, \"fullName\": \"Alice Johnson\", \"address\": \"789 Satoshi St\", \"quantity\": 13000, \"pricePerOunce\": 1300, \"totalAmount\": 1300, \"transactionType\": \"BUY\", \"createdAt\": \"1712847205\", \"confirmingParty\": \"DEF Inc\", \"goldUnit\": \"OZ\", \"signatureActionParty\": \"1234\", \"signatureConfirmingParty\": \"SIGNED\", \"contractStatus\": \"APPROVED\"}");
//        inOrder.verify(stub).putStringState("5L", "{\"contractId\": 5, \"actionParty\": 3, \"fullName\": \"John Smith\", \"address\": \"1010 Ether Ave\", \"quantity\": 14000, \"pricePerOunce\": 1400, \"totalAmount\": 1400, \"transactionType\": \"BUY\", \"createdAt\": \"1712847210\", \"confirmingParty\": \"GHI LLC\", \"goldUnit\": \"KG\", \"signatureActionParty\": \"5678\", \"signatureConfirmingParty\": \"AUTHORIZED\", \"contractStatus\": \"VALIDATED\"}");
//
    }

    @Nested
    class InvokeCreateAssetTransaction {

//        @Test
//        public void whenAssetExists() {
//            AssetTransfer contract = new AssetTransfer();
//            Context ctx = mock(Context.class);
//            ChaincodeStub stub = mock(ChaincodeStub.class);
//            when(ctx.getStub()).thenReturn(stub);
//            when(stub.getStringState("1L"))
//                    .thenReturn("{ \"contractId\": 1L, \"actionParty\": 1L, \"fullName\": \"Tomoko\", \"address\": \"123 Blockchain Blvd\", \"quantity\": 12510, \"pricePerOunce\": 1250, \"totalAmount\": 1250, \"transactionType\": \"BUY\", \"createdAt\": \"1712847198\", \"confirmingParty\": \"BGSS Company\", \"goldUnit\": \"ABDC\", \"signatureActionParty\": \"CDBA\", \"signatureConfirmingParty\": \"SIGNED\", \"contractStatus\": \"DIGITAL_SIGNED\" }");
//
//            Throwable thrown = catchThrowable(() -> {
//                BigDecimal quantity = new BigDecimal(12510);
//                BigDecimal pricePerOunce = new BigDecimal(1250);
//                BigDecimal totalAmount = new BigDecimal(1250);
//                contract.CreateAsset(ctx, 1L, 1L, "Tomoko", "123 Blockchain Blvd", quantity, pricePerOunce, totalAmount, "BUY", new Date(1712847198), "BGSS Company", "ABCD", "CDBA", "SIGNED", "DIGITAL_SIGNED");
//            });
//
//            assertThat(thrown).isInstanceOf(ChaincodeException.class).hasNoCause()
//                    .hasMessage("Asset asset1 already exists");
//            assertThat(((ChaincodeException) thrown).getPayload()).isEqualTo("ASSET_ALREADY_EXISTS".getBytes());
//        }

//        @Test
//        public void whenAssetDoesNotExist() {
//            AssetTransfer contract = new AssetTransfer();
//            Context ctx = mock(Context.class);
//            ChaincodeStub stub = mock(ChaincodeStub.class);
//            when(ctx.getStub()).thenReturn(stub);
//            when(stub.getStringState("asset1")).thenReturn("");
//            BigDecimal quantity = new BigDecimal(12510);
//            BigDecimal pricePerOunce = new BigDecimal(1250);
//            BigDecimal totalAmount = new BigDecimal(1250);
//            ContractDTO asset = contract.CreateAsset(ctx, 1L, 1L, "Tomoko", "123 Blockchain Blvd", quantity, pricePerOunce, totalAmount, "BUY", new Date(1712847198), "BGSS Company", "ABCD", "CDBA", "SIGNED", "DIGITAL_SIGNED");
//
//            assertThat(asset).isEqualTo(new ContractDTO("asset1",1L, 1L, "Tomoko", "123 Blockchain Blvd", quantity, pricePerOunce, totalAmount, "BUY", new Date(1712847198), "BGSS Company", "ABCD", "CDBA", "SIGNED", "DIGITAL_SIGNED"));
//        }
    }

//    @Test
//    void invokeGetAllAssetsTransaction() {
//        AssetTransfer contract = new AssetTransfer();
//        Context ctx = mock(Context.class);
//        ChaincodeStub stub = mock(ChaincodeStub.class);
//        when(ctx.getStub()).thenReturn(stub);
//        when(stub.getStateByRange("", "")).thenReturn(new MockAssetResultsIterator());
//
//        String contractId = contract.GetAllAssets(ctx);
//
//        assertThat(contractId).isEqualTo("[{\"contractId\": 1L, \"actionParty\": 1L, \"fullName\": \"Tomoko\", \"address\": \"123 Blockchain Blvd\", \"quantity\": 12510, \"pricePerOunce\": 1250, \"totalAmount\": 1250, \"transactionType\": \"BUY\", \"createdAt\": \"1712847198\", \"confirmingParty\": \"BGSS Company\", \"goldUnit\": \"ABDC\", \"signatureActionParty\": \"CDBA\", \"signatureConfirmingParty\": \"SIGNED\", \"contractStatus\": \"DIGITAL_SIGNED\"},"
//                + "{\"contractId\": 2L, \"actionParty\": 2L, \"fullName\": \"Tomoko\", \"address\": \"123 Blockchain Blvd\", \"quantity\": 12510, \"pricePerOunce\": 1250, \"totalAmount\": 1250, \"transactionType\": \"BUY\", \"createdAt\": \"1712847198\", \"confirmingParty\": \"BGSS Company\", \"goldUnit\": \"ABDC\", \"signatureActionParty\": \"CDBA\", \"signatureConfirmingParty\": \"SIGNED\", \"contractStatus\": \"DIGITAL_SIGNED\"},"
//                + "{\"contractId\": 3L, \"actionParty\": 2L, \"fullName\": \"Jane Doe\", \"address\": \"456 Crypto Road\", \"quantity\": 15000, \"pricePerOunce\": 1500, \"totalAmount\": 1500, \"transactionType\": \"SELL\", \"createdAt\": \"1712847200\", \"confirmingParty\": \"XYZ Corp\", \"goldUnit\": \"GRAMS\", \"signatureActionParty\": \"ABCD\", \"signatureConfirmingParty\": \"CONFIRMED\", \"contractStatus\": \"PENDING\"},"
//                + "{\"contractId\": 4L, \"actionParty\": 1L, \"fullName\": \"Alice Johnson\", \"address\": \"789 Satoshi St\", \"quantity\": 13000, \"pricePerOunce\": 1300, \"totalAmount\": 1300, \"transactionType\": \"BUY\", \"createdAt\": \"1712847205\", \"confirmingParty\": \"DEF Inc\", \"goldUnit\": \"OZ\", \"signatureActionParty\": \"1234\", \"signatureConfirmingParty\": \"SIGNED\", \"contractStatus\": \"APPROVED\"},"
//                + "{\"contractId\": 5L, \"actionParty\": 3L, \"fullName\": \"John Smith\", \"address\": \"1010 Ether Ave\", \"quantity\": 14000, \"pricePerOunce\": 1400, \"totalAmount\": 1400, \"transactionType\": \"BUY\", \"createdAt\": \"1712847210\", \"confirmingParty\": \"GHI LLC\", \"goldUnit\": \"KG\", \"signatureActionParty\": \"5678\", \"signatureConfirmingParty\": \"AUTHORIZED\", \"contractStatus\": \"VALIDATED\"},"
//                + "{\"contractId\": 6L, \"actionParty\": 5L, \"fullName\": \"Mohammed Ali\", \"address\": \"1212 Blockchain Loop\", \"quantity\": 13500, \"pricePerOunce\": 1350, \"totalAmount\": 1350, \"transactionType\": \"SELL\", \"createdAt\": \"1712847215\", \"confirmingParty\": \"JKL Holdings\", \"goldUnit\": \"LB\", \"signatureActionParty\": \"91011\", \"signatureConfirmingParty\": \"VERIFIED\", \"contractStatus\": \"COMPLETED\"}");
//
//    }

    @Nested
    class TransferAssetTransaction {

//        @Test
//        public void whenAssetExists() {
//            AssetTransfer contract = new AssetTransfer();
//            Context ctx = mock(Context.class);
//            ChaincodeStub stub = mock(ChaincodeStub.class);
//            when(ctx.getStub()).thenReturn(stub);
//            when(stub.getStringState("1L"))
//                    .thenReturn("{ \"contractId\": 1L, \"actionParty\": 1L, \"fullName\": \"Tomoko\", \"address\": \"123 Blockchain Blvd\", \"quantity\": 12510, \"pricePerOunce\": 1250, \"totalAmount\": 1250, \"transactionType\": \"BUY\", \"createdAt\": \"1712847198\", \"confirmingParty\": \"BGSS Company\", \"goldUnit\": \"ABDC\", \"signatureActionParty\": \"CDBA\", \"signatureConfirmingParty\": \"SIGNED\", \"contractStatus\": \"DIGITAL_SIGNED\"} ");
//
//            String oldOwner = contract.TransferAsset(ctx, "1L", "Dr Evil");
//
//            assertThat(oldOwner).isEqualTo("Tomoko");
//        }
//
//        @Test
//        public void whenAssetDoesNotExist() {
//            AssetTransfer contract = new AssetTransfer();
//            Context ctx = mock(Context.class);
//            ChaincodeStub stub = mock(ChaincodeStub.class);
//            when(ctx.getStub()).thenReturn(stub);
//            when(stub.getStringState("asset1")).thenReturn("");
//
//            Throwable thrown = catchThrowable(() -> {
//                contract.TransferAsset(ctx, "1L", "Dr Evil");
//            });
//
//            assertThat(thrown).isInstanceOf(ChaincodeException.class).hasNoCause()
//                    .hasMessage("Asset asset1 does not exist");
//            assertThat(((ChaincodeException) thrown).getPayload()).isEqualTo("ASSET_NOT_FOUND".getBytes());
//        }
    }

    @Nested
    class UpdateAssetTransaction {

//        @Test
//        public void whenAssetExists() {
//            AssetTransfer contract = new AssetTransfer();
//            Context ctx = mock(Context.class);
//            ChaincodeStub stub = mock(ChaincodeStub.class);
//            when(ctx.getStub()).thenReturn(stub);
//            when(stub.getStringState("asset1"))
//                    .thenReturn("{\"contractId\": 1L, \"actionParty\": 1L, \"fullName\": \"Tomoko\", \"address\": \"123 Blockchain Blvd\", \"quantity\": 12510, \"pricePerOunce\": 12510, \"totalAmount\": 1250, \"transactionType\": \"BUY\", \"createdAt\": \"1712847198\", \"confirmingParty\": \"BGSS Company\", \"goldUnit\": \"GAM\", \"signatureActionParty\": \"CDBA\", \"signatureConfirmingParty\": \"SIGNED\", \"contractStatus\": \"DIGITAL_SIGNED\"}");
//
//            BigDecimal quantity = new BigDecimal(12510);
//            BigDecimal pricePerOunce = new BigDecimal(12510);
//            BigDecimal totalAmount = new BigDecimal(1250);
//
//            ContractDTO asset = contract.ReadAsset(ctx, "1L");
//
//            assertThat(asset).isEqualTo(new ContractDTO(1L, 1L, "Tomoko", "123 Blockchain Blvd", quantity, pricePerOunce, totalAmount, "BUY", new Date(), "BGSS Company", "GAM", "CDBA", "SIGNED", "DIGITAL_SIGNED"));
//
//        }
//
//        @Test
//        public void whenAssetDoesNotExist() {
//            AssetTransfer contract = new AssetTransfer();
//            Context ctx = mock(Context.class);
//            ChaincodeStub stub = mock(ChaincodeStub.class);
//            when(ctx.getStub()).thenReturn(stub);
//            when(stub.getStringState("asset1")).thenReturn("");
//
//            Throwable thrown = catchThrowable(() -> {
//                contract.TransferAsset(ctx, "1L", "Alex");
//            });
//
//            assertThat(thrown).isInstanceOf(ChaincodeException.class).hasNoCause()
//                    .hasMessage("Asset asset1 does not exist");
//            assertThat(((ChaincodeException) thrown).getPayload()).isEqualTo("ASSET_NOT_FOUND".getBytes());
//        }
    }

    @Nested
    class DeleteAssetTransaction {

        @Test
        public void whenAssetDoesNotExist() {
            ContractBGSS contract = new ContractBGSS();
            Context ctx = mock(Context.class);
            ChaincodeStub stub = mock(ChaincodeStub.class);
            when(ctx.getStub()).thenReturn(stub);
            when(stub.getStringState("asset1")).thenReturn("");

            Throwable thrown = catchThrowable(() -> {
                contract.DeleteAsset(ctx, "asset1");
            });

            assertThat(thrown).isInstanceOf(ChaincodeException.class).hasNoCause()
                    .hasMessage("Asset asset1 does not exist");
            assertThat(((ChaincodeException) thrown).getPayload()).isEqualTo("ASSET_NOT_FOUND".getBytes());
        }
    }
}
