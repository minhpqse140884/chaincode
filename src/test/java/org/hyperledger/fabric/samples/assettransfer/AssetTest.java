///*
// * SPDX-License-Identifier: Apache-2.0
// */
//
//package org.hyperledger.fabric.samples.assettransfer;
//
//import static org.assertj.core.api.Assertions.assertThat;
//
//import org.hyperledger.fabric.samples.BGSSContract.ContractDTO;
//import org.junit.jupiter.api.Nested;
//import org.junit.jupiter.api.Test;
//
//import java.math.BigDecimal;
//import java.util.Date;
//
//public final class AssetTest {
//
//    @Nested
//    class Equality {
//
////        @Test
////        public void isReflexive() {
////            ContractDTO asset = new ContractDTO("asset1",1L, 2L, "John Doe", "123 Blockchain Blvd",
////                    new BigDecimal("100"), new BigDecimal("1500.00"), new BigDecimal("150000.00"),
////                    "Purchase", new Date(), "Company XYZ", "grams",
////                    "signatureOfJohnDoe", "signatureOfCompanyXYZ", "Active");
////            assertThat(asset).isEqualTo(asset);
////        }
//
////        @Test
////        public void isSymmetric() {
////            ContractDTO assetA = new ContractDTO("asset1",1L, 2L, "John Doe",
////                    "123 Blockchain Blvd", new BigDecimal("100"), new BigDecimal("1500.00"), new BigDecimal("150000.00"), "Purchase", new Date(), "Company XYZ", "grams", "signatureOfJohnDoe", "signatureOfCompanyXYZ", "Active");
////            ContractDTO assetB = new ContractDTO("asset1",1L, 2L, "John Doe",
////                    "123 Blockchain Blvd", new BigDecimal("100"), new BigDecimal("1500.00"),
////                    new BigDecimal("150000.00"), "Purchase", new Date(), "Company XYZ",
////                    "grams", "signatureOfJohnDoe", "signatureOfCompanyXYZ",
////                    "Active");
////            assertThat(assetA).isEqualTo(assetB);
////            assertThat(assetB).isEqualTo(assetA);
////        }
//
////        @Test
////        public void isTransitive() {
////            ContractDTO assetA = new ContractDTO("asset1",1L, 2L, "John Doe", "123 Blockchain Blvd", new BigDecimal("100"), new BigDecimal("1500.00"), new BigDecimal("150000.00"), "Purchase", new Date(), "Company XYZ", "grams", "signatureOfJohnDoe", "signatureOfCompanyXYZ", "Active");
////            ContractDTO assetB = new ContractDTO("asset1",1L, 2L, "John Doe", "123 Blockchain Blvd", new BigDecimal("100"), new BigDecimal("1500.00"), new BigDecimal("150000.00"), "Purchase", new Date(), "Company XYZ", "grams", "signatureOfJohnDoe", "signatureOfCompanyXYZ", "Active");
////            ContractDTO assetC = new ContractDTO("asset1",1L, 2L, "John Doe", "123 Blockchain Blvd", new BigDecimal("100"), new BigDecimal("1500.00"), new BigDecimal("150000.00"), "Purchase", new Date(), "Company XYZ", "grams", "signatureOfJohnDoe", "signatureOfCompanyXYZ", "Active");
////            assertThat(assetA).isEqualTo(assetB);
////            assertThat(assetB).isEqualTo(assetC);
////            assertThat(assetA).isEqualTo(assetC);
////        }
//
//        @Test
//        public void handlesInequality() {
//            ContractDTO assetA = new ContractDTO("asset1",1L, 2L, "John Doe", "123 Blockchain Blvd", new BigDecimal("100"), new BigDecimal("1500.00"), new BigDecimal("150000.00"), "Purchase", new Date(), "Company XYZ", "grams", "signatureOfJohnDoe", "signatureOfCompanyXYZ", "Active");
//            ContractDTO assetB = new ContractDTO("asset2",2L, 3L, "Jane Doe", "456 Blockchain Blvd", new BigDecimal("200"), new BigDecimal("1600.00"), new BigDecimal("320000.00"), "Sale", new Date(), "Company ABC", "grams", "signatureOfJaneDoe", "signatureOfCompanyABC", "Inactive");
//            assertThat(assetA).isNotEqualTo(assetB);
//        }
//
//        @Test
//        public void handlesOtherObjects() {
//            ContractDTO assetA = new ContractDTO("asset1",1L, 2L, "John Doe", "123 Blockchain Blvd", new BigDecimal("100"), new BigDecimal("1500.00"), new BigDecimal("150000.00"), "Purchase", new Date(), "Company XYZ", "grams", "signatureOfJohnDoe", "signatureOfCompanyXYZ", "Active");
//            String notAContractDTO = "not a ContractDTO";
//            assertThat(assetA).isNotEqualTo(notAContractDTO);
//        }
//
//        @Test
//        public void handlesNull() {
//            ContractDTO asset = new ContractDTO("asset1",1L, 2L, "John Doe", "123 Blockchain Blvd",
//                    new BigDecimal("100"), new BigDecimal("1500.00"), new BigDecimal("150000.00"),
//                    "Purchase", new Date(), "Company XYZ", "grams",
//                    "signatureOfJohnDoe", "signatureOfCompanyXYZ", "Active");
//            assertThat(asset).isNotEqualTo(null);
//        }
//    }
//
//    @Test
//    public void toStringIdentifiesAsset() {
//        Date now = new Date();
//        ContractDTO asset = new ContractDTO("asset1",1L, 2L, "John Doe", "123 Blockchain Blvd",
//                new BigDecimal("100"), new BigDecimal("1500.00"), new BigDecimal("150000.00"),
//                "Purchase", now, "Company XYZ", "grams",
//                "signatureOfJohnDoe", "signatureOfCompanyXYZ", "Active");
//        String expectedToString = "ContractDTO@" + Integer.toHexString(asset.hashCode()) +
//                " [assetID=asset1, contractId=1, actionParty=2, fullName=John Doe, address=123 Blockchain Blvd, quantity=100, pricePerOunce=1500.00, totalCostOrProfit=150000.00, transactionType=Purchase, createdAt=" + now +
//                ", confirmingParty=Company XYZ, goldUnit=grams, signatureActionParty=signatureOfJohnDoe, signatureConfirmingParty=signatureOfCompanyXYZ, contractStatus=Active]";
//        assertThat(asset.toString()).isEqualTo(expectedToString);
//    }
//}
