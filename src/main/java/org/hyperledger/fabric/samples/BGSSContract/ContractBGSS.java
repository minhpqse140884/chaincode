/*
 * SPDX-License-Identifier: Apache-2.0
 */

package org.hyperledger.fabric.samples.BGSSContract;

import com.owlike.genson.Genson;
import org.hyperledger.fabric.Logger;
import org.hyperledger.fabric.contract.Context;
import org.hyperledger.fabric.contract.ContractInterface;
import org.hyperledger.fabric.contract.annotation.*;
import org.hyperledger.fabric.shim.ChaincodeException;
import org.hyperledger.fabric.shim.ChaincodeStub;
import org.hyperledger.fabric.shim.ledger.KeyValue;
import org.hyperledger.fabric.shim.ledger.QueryResultsIterator;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

@Contract(
        name = "contract",
        info = @Info(
                title = "Asset Transfer",
                description = "The hyperlegendary asset transfer",
                version = "0.0.1-SNAPSHOT",
                license = @License(
                        name = "Apache 2.0 License",
                        url = "http://www.apache.org/licenses/LICENSE-2.0.html"),
                contact = @Contact(
                        email = "a.transfer@example.com",
                        name = "Adrian Transfer",
                        url = "https://hyperledger.example.com")))
@Default
public final class ContractBGSS implements ContractInterface {

    private final Genson genson = new Genson();

    private static final Logger logger = Logger.getLogger(ContractBGSS.class.getName());

    private enum AssetTransferErrors {
        ASSET_NOT_FOUND,
        ASSET_ALREADY_EXISTS
    }

    @Transaction(intent = Transaction.TYPE.SUBMIT)
    public void InitLedger(final Context ctx) {
        ChaincodeStub stub = ctx.getStub();
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date fixedDate = dateFormat.parse("2023-01-01");

//            createAsset(ctx, "asset1", 1, 1, "John Doe", "123 Blockchain Blvd",
//                    new BigDecimal("100"), new BigDecimal("1500.00"), new BigDecimal("150000.00"),
//                    "Purchase", fixedDate, "Company XYZ", "grams",
//                    "signatureOfJohnDoe", "signatureOfCompanyXYZ", "Active");
        } catch (Exception e) {
            System.err.println("Error parsing the fixed date: " + e.getMessage());
        }
    }

    private ChaincodeStub chaincodeStub(final Context ctx) {
        return ctx.getStub();
    }

    private String getAssetKey(final long contractId) {
        return "asset" + contractId;
    }

    @Transaction(intent = Transaction.TYPE.SUBMIT)
    public ContractDTO createAsset(Context ctx, String assetID, String contractId, String actionParty,
                                   String fullName, String address, String quantity,
                                   String pricePerOunce, String totalCostOrProfit,
                                   String transactionType, String createdAt, String confirmingParty,
                                   String goldUnit, String signatureActionParty,
                                   String signatureConfirmingParty, String contractStatus) {
        logger.info("Start executing CreateAsset function");
        logger.info(String.format("ctx: %s", ctx));
        logger.info(String.format("ContractId: %s", contractId));
        logger.info(String.format("ActionParty: %s", actionParty));
        logger.info(String.format("FullName: %s", fullName));
        logger.info(String.format("Address: %s", address));
        logger.info(String.format("Quantity: %s", quantity));
        logger.info(String.format("PricePerOunce: %s", pricePerOunce));
        logger.info(String.format("TotalCostOrProfit: %s", totalCostOrProfit));
        logger.info(String.format("TransactionType: %s", transactionType));
        logger.info(String.format("CreatedAt: %s", createdAt));
        logger.info(String.format("ConfirmingParty: %s", confirmingParty));
        logger.info(String.format("GoldUnit: %s", goldUnit));
        logger.info(String.format("SignatureActionParty: %s", signatureActionParty));
        logger.info(String.format("SignatureConfirmingParty: %s", signatureConfirmingParty));
        logger.info(String.format("ContractStatus: %s", contractStatus));

        long contractIdLong = Long.parseLong(contractId);
        long actionPartyLong = Long.parseLong(actionParty);
        BigDecimal quantityDecimal = new BigDecimal(quantity);
        BigDecimal pricePerOunceDecimal = new BigDecimal(pricePerOunce);
        BigDecimal totalCostOrProfitDecimal = new BigDecimal(totalCostOrProfit);
        Date createDate = parseDate(createdAt);
        ChaincodeStub stub = ctx.getStub();

        logger.info(String.format("Created asset key: %s", assetID));
        boolean exists = AssetExists(ctx, assetID);
        if (exists) {
            String errorMessage = String.format("Asset %s already exists", assetID);
            logger.severe(errorMessage);
            throw new ChaincodeException(errorMessage, AssetTransferErrors.ASSET_ALREADY_EXISTS.toString());
        }
        ContractDTO contract = getContractDTO(assetID, contractIdLong, actionPartyLong, fullName, address, quantityDecimal,
                pricePerOunceDecimal, totalCostOrProfitDecimal, transactionType, createDate, confirmingParty, goldUnit, signatureActionParty,
                signatureConfirmingParty, contractStatus);
        String sortedJson = genson.serialize(contract);
        logger.info(sortedJson);
        stub.putStringState(assetID, sortedJson);
        logger.info(String.format("Asset with ID %s is created successfully", assetID));
        return contract;
    }

    private Date parseDate(String dateString) {
        try {
            SimpleDateFormat isoDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            isoDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            return isoDateFormat.parse(dateString);
        } catch (Exception e) {
            logger.severe("Error parsing date: " + e.getMessage());
            return null; // Or handle more appropriately
        }
    }

    private static ContractDTO getContractDTO(String createAssetKey, long contractId, long actionParty,
                                              String fullName, String address, BigDecimal quantity,
                                              BigDecimal pricePerOunce, BigDecimal totalCostOrProfit,
                                              String transactionType, Date createdAt, String confirmingParty,
                                              String goldUnit, String signatureActionParty,
                                              String signatureConfirmingParty, String contractStatus) {
        ContractDTO contract = new ContractDTO();
        contract.setAssetID(createAssetKey);
        contract.setContractId(contractId);
        contract.setActionParty(actionParty);
        contract.setFullName(fullName);
        contract.setAddress(address);
        contract.setQuantity(quantity);
        contract.setPricePerOunce(pricePerOunce);
        contract.setTotalCostOrProfit(totalCostOrProfit);
        contract.setTransactionType(transactionType);
        contract.setCreatedAt(createdAt);
        contract.setConfirmingParty(confirmingParty);
        contract.setGoldUnit(goldUnit);
        contract.setSignatureActionParty(signatureActionParty);
        contract.setSignatureConfirmingParty(signatureConfirmingParty);
        contract.setContractStatus(contractStatus);
        return contract;
    }

    @Transaction(intent = Transaction.TYPE.EVALUATE)
    public ContractDTO ReadAsset(final Context ctx, final String assetID) {
        ChaincodeStub stub = chaincodeStub(ctx);
        String assetJson = stub.getStringState(assetID);

        if (assetJson == null || assetJson.isEmpty()) {
            String errorMessage = String.format("Asset %s does not exist", assetID);
            System.out.println(errorMessage);
            throw new ChaincodeException(errorMessage, AssetTransferErrors.ASSET_NOT_FOUND.toString());
        }

        return genson.deserialize(assetJson, ContractDTO.class);
    }

//    @Transaction(intent = Transaction.TYPE.SUBMIT)
//    public ContractDTO UpdateAsset(final Context ctx, final long contractId, final long actionParty,
//                                   final String fullName, final String address, final BigDecimal quantity,
//                                   final BigDecimal pricePerOunce, final BigDecimal totalCostOrProfit,
//                                   final String transactionType, final Date createdAt, final String confirmingParty,
//                                   final String goldUnit, final String signatureActionParty,
//                                   final String signatureConfirmingParty, final String contractStatus) {
//        ChaincodeStub stub = ctx.getStub();
//        String assetKey = "asset" + contractId;
//        if (AssetExists(ctx, assetKey)) {
//            String errorMessage = String.format("Asset %s already exists", assetKey);
//            System.out.println(errorMessage);
//            throw new ChaincodeException(errorMessage, AssetTransferErrors.ASSET_ALREADY_EXISTS.toString());
//        }
//
//        ContractDTO contract = getContractDTO(assetKey, contractId, actionParty, fullName, address, quantity,
//                pricePerOunce, totalCostOrProfit, transactionType, createdAt, confirmingParty, goldUnit, signatureActionParty,
//                signatureConfirmingParty, contractStatus);
//
//        ctx.getStub().putState(assetKey, contract.toJSONString().getBytes(UTF_8));
//
//        return contract;
//    }

    @Transaction(intent = Transaction.TYPE.SUBMIT)
    public void DeleteAsset(final Context ctx, final String assetID) {
        ChaincodeStub stub = ctx.getStub();

        if (!AssetExists(ctx, assetID)) {
            String errorMessage = String.format("Asset %s does not exist", assetID);
            System.out.println(errorMessage);
            throw new ChaincodeException(errorMessage, AssetTransferErrors.ASSET_NOT_FOUND.toString());
        }

        stub.delState(assetID);
    }

    /**
     * Checks the existence of the asset on the ledger
     *
     * @param ctx     the transaction context
     * @param assetID the ID of the asset
     * @return boolean indicating the existence of the asset AssetExists
     */
    @Transaction()
    public boolean AssetExists(Context ctx, String assetID) {
        byte[] buffer = ctx.getStub().getState(assetID);
        return (buffer != null && buffer.length > 0);
    }

//    @Transaction(intent = Transaction.TYPE.SUBMIT)
//    public String TransferAsset(final Context ctx, final String assetID, final String newConfirmingParty) {
//        ChaincodeStub stub = ctx.getStub();
//        String assetJson = stub.getStringState(assetID);
//
//        if (assetJson == null || assetJson.isEmpty()) {
//            String errorMessage = String.format("Asset %s does not exist", assetID);
//            System.out.println(errorMessage);
//            throw new ChaincodeException(errorMessage, AssetTransferErrors.ASSET_NOT_FOUND.toString());
//        }
//
//        ContractDTO asset = genson.deserialize(assetJson, ContractDTO.class);
//
//        ContractDTO contractDTO = new ContractDTO(assetJson, asset.getContractId(), asset.getActionParty(), asset.getFullName(), asset.getAddress(),
//                asset.getQuantity(), asset.getPricePerOunce(), asset.getTotalCostOrProfit(), asset.getTransactionType(), asset.getCreatedAt(),
//                asset.getConfirmingParty(), asset.getGoldUnit(), asset.getSignatureActionParty(), asset.getConfirmingParty(), asset.getContractStatus());
//
//        stub.putStringState(assetID, genson.serialize(contractDTO));
//
//        return asset.getConfirmingParty();
//    }


    @Transaction(intent = Transaction.TYPE.EVALUATE)
    public String GetAllAssets(final Context ctx) {
        ChaincodeStub stub = ctx.getStub();
        List<ContractDTO> queryResults = new ArrayList<ContractDTO>();

        QueryResultsIterator<KeyValue> results = stub.getStateByRange("", "");

        for (KeyValue result : results) {
            ContractDTO asset = genson.deserialize(result.getStringValue(), ContractDTO.class);
            System.out.println(asset);
            queryResults.add(asset);
        }

        final String response = genson.serialize(queryResults);
        return response;
    }
}
