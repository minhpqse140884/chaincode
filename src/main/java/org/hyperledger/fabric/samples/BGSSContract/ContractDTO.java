/*
 * SPDX-License-Identifier: Apache-2.0
 */
package org.hyperledger.fabric.samples.BGSSContract;

import org.hyperledger.fabric.Logger;
import org.hyperledger.fabric.contract.annotation.DataType;
import org.hyperledger.fabric.contract.annotation.Property;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.Date;

@DataType()
public final class ContractDTO {

    @Property()
    private String assetID;

    @Property()
    private long contractId;

    @Property()
    private long actionParty;

    @Property()
    private String fullName;

    @Property()
    private String address;

    @Property()
    private  BigDecimal quantity;

    @Property()
    private  BigDecimal pricePerOunce;

    @Property()
    private  BigDecimal totalCostOrProfit;

    @Property()
    private  String transactionType;

    @Property()
    private  Date createdAt;

    @Property()
    private  String confirmingParty;

    @Property()
    private  String goldUnit;

    @Property()
    private  String signatureActionParty;

    @Property()
    private  String signatureConfirmingParty;

    @Property()
    private  String contractStatus;

//    @JsonProperty("assetID")
    public String getAssetID() {
        return assetID;
    }

//    @JsonProperty("contractId")
    public long getContractId() {
        return contractId;
    }

//    @JsonProperty("actionParty")
    public long getActionParty() {
        return actionParty;
    }

//    @JsonProperty("fullName")
    public String getFullName() {
        return fullName;
    }

//    @JsonProperty("address")
    public String getAddress() {
        return address;
    }

//    @JsonProperty("quantity")
    public BigDecimal getQuantity() {
        return quantity;
    }

//    @JsonProperty("pricePerOunce")
    public BigDecimal getPricePerOunce() {
        return pricePerOunce;
    }

//    @JsonProperty("totalCostOrProfit")
    public BigDecimal getTotalCostOrProfit() {
        return totalCostOrProfit;
    }

//    @JsonProperty("transactionType")
    public String getTransactionType() {
        return transactionType;
    }

//    @JsonProperty("createdAt")
    public Date getCreatedAt() {
        return createdAt;
    }

//    @JsonProperty("confirmingParty")
    public String getConfirmingParty() {
        return confirmingParty;
    }

//    @JsonProperty("goldUnit")
    public String getGoldUnit() {
        return goldUnit;
    }

//    @JsonProperty("signatureActionParty")
    public String getSignatureActionParty() {
        return signatureActionParty;
    }

//    @JsonProperty("signatureConfirmingParty")
    public String getSignatureConfirmingParty() {
        return signatureConfirmingParty;
    }

//    @JsonProperty("contractStatus")
    public String getContractStatus() {
        return contractStatus;
    }

    public void setAssetID(String assetID) {
        this.assetID = assetID;
    }

    public void setContractId(long contractId) {
        this.contractId = contractId;
    }

    public void setActionParty(long actionParty) {
        this.actionParty = actionParty;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public void setPricePerOunce(BigDecimal pricePerOunce) {
        this.pricePerOunce = pricePerOunce;
    }

    public void setTotalCostOrProfit( BigDecimal totalCostOrProfit) {
        this.totalCostOrProfit = totalCostOrProfit;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public void setConfirmingParty( String confirmingParty) {
        this.confirmingParty = confirmingParty;
    }

    public void setGoldUnit(String goldUnit) {
        this.goldUnit = goldUnit;
    }

    public void setSignatureActionParty(String signatureActionParty) {
        this.signatureActionParty = signatureActionParty;
    }

    public void setSignatureConfirmingParty(String signatureConfirmingParty) {
        this.signatureConfirmingParty = signatureConfirmingParty;
    }

    public void setContractStatus(String contractStatus) {
        this.contractStatus = contractStatus;
    }


    public ContractDTO() {
    }


    private static final Logger logger = Logger.getLogger(ContractDTO.class.getName());


}
